# ukljucivanje biblioteke pygame
import pygame
import random


pygame.init()
pygame.font.init()

# definiranje konstanti za velicinu prozora
WIDTH = 1600
HEIGHT = 900

BLACK = [0, 0, 0]
YELLOW = [255, 255, 0]
RED = [255, 0, 0]
GREEN = [0, 255, 0]
BLUE = [0, 0, 255]
TIRKIZ = [64, 224, 208]
MAGENTA = [255, 0, 255]
WHITE=[255,255,255]
# tuple velicine prozora
size = (WIDTH, HEIGHT)

# definiranje novog ekrana za igru
screen = pygame.display.set_mode(size)
# definiranje naziva prozora
pygame.display.set_caption("Nasa kul igra")

bg = pygame.image.load("bg.jpg")
bg = pygame.transform.scale(bg, (WIDTH, HEIGHT))
icon = pygame.image.load("icon.png")
icon = pygame.transform.scale(icon, (100, 100))
myfont = pygame.font.SysFont('Arial', 40)
scorefont = pygame.font.SysFont('Arial', 72)
timefont=pygame.font.SysFont('Arial', 24)
start_text = myfont.render('Press SPACE to play!', False, RED)

state = "start"  # "play", "score"
clock = pygame.time.Clock()
i = 0
duration = 3000
bg_color = TIRKIZ
done = False
iconx = random.randint(0, WIDTH-100)
icony = random.randint(0, HEIGHT-100)
score=0
while not done:
    #print(score)
    #print(state)
    # event petlja
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            done = True
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_SPACE:
                if state == "start":
                    state = "play"
                    timeleft=duration
        if event.type == pygame.MOUSEBUTTONDOWN:
            if iconpos.collidepoint(event.pos):
                iconx = random.randint(0, WIDTH-100)
                icony = random.randint(0, HEIGHT-100)
                timeleft=duration
                duration*=0.95
                print(duration)
                score+=1

    # state change
    if state=="play":
        timeleft-=time
        if timeleft<=0:
            state="score"
    # iscrtavanje
    if state == "start":
        screen.blit(bg, (0, 0))
        screen.blit(start_text, (30, 30))
    elif state == "play":
        screen.fill(WHITE)
        score_text=scorefont.render("Score: %d"%score, False, RED )
        score_width=score_text.get_width()
        time_text=timefont.render("Time: %.2f"%(timeleft/1000),False, BLUE)
        time_height=time_text.get_height()
        iconpos=screen.blit(icon, (iconx,icony))
        screen.blit(score_text, (WIDTH-score_width-10, 10))
        screen.blit(time_text, (10,HEIGHT-time_height-10))
    elif state=="score":
        screen.fill(RED)
    pygame.display.flip()

    # ukoliko je potrebno ceka do iscrtavanja
    # iduceg framea kako bi imao 60fpsa
    time = clock.tick(60)
    #duration = duration-time
pygame.quit()
