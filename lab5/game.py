# ukljucivanje biblioteke pygame
import pygame


def next_bg(color):
    if color==tamnocijan:
        color=RED
    elif color==RED:
        color=magenta
    elif color==magenta:
        color=TIRKIZ
    elif color==TIRKIZ:
        color=BLUE
    elif color==BLUE:
        color=tamnocijan
    return color
def next_img(image):
    if image==bg_ing:
        image=bg_ing1
    elif image==bg_ing1:
        image=bg_ing2
    elif image==bg_ing2:
        image=bg_ing
    return image
pygame.init()
# definiranje konstanti za velicinu prozora
WIDTH = 1024
HEIGHT = 600
RED=[255,0,0]
GREEN=[0,255,0]
BLUE=[0,0,255]
TIRKIZ=[64,224,208]
tamnocijan=[0,139,139]
magenta=[255,0,255]

bg_ing=pygame.image.load("bg.jpeg")
bg_ing=pygame.transform.scale(bg_ing,(WIDTH,HEIGHT))
bg_ing1=pygame.image.load("bg1.jpeg")
bg_ing1=pygame.transform.scale(bg_ing1,(WIDTH,HEIGHT))
bg_ing2=pygame.image.load("bg2.jpeg")
bg_ing2=pygame.transform.scale(bg_ing2,(WIDTH,HEIGHT))

# tuple velicine prozora
size = (WIDTH, HEIGHT)
 
#definiranje novog ekrana za igru
screen = pygame.display.set_mode(size)
#definiranje naziva prozora
pygame.display.set_caption("Nasa kul igra")
 
clock = pygame.time.Clock()
i=0
duration=1000
bg_color=tamnocijan
bg=bg_ing
done = False
while not done:
    #event petlja
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            done = True
        if event.type ==pygame.KEYDOWN:
            if event.key==pygame.K_SPACE:
                bg_color=next_bg(bg_color)               
    i+=1
    if duration<0:
        duration=1000
        bg=next_img(bg)
        

    screen.fill(bg_color)
    screen.blit(bg,(0,0))
    pygame.display.flip()
   
    #ukoliko je potrebno ceka do iscrtavanja
    #iduceg framea kako bi imao 60fpsa
    time= clock.tick(60)
    duration=duration-time
    #print(duration)
pygame.quit()