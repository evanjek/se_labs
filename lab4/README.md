ls => prikaži sadržaj direktorija

cd DIREKTORIJ => ulaz u direktorij

git clone REPOZITORIJ => povuci sa interneta

git status => pokaži promjene

git log => prikaz trenutnih datoteka

git add IME_FILEA => dodaj folder

git commit -m "PORUKA" => komitaj file

git push origin master => dodaj to na internet
