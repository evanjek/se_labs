# ukljucivanje biblioteke pygame
import pygame
import random

def next_img(image):
    if image==bg_img:
        image=bg_img1
    elif image==bg_img1:
        image=bg_img2
    elif image==bg_img2:
        image=bg_img3
    elif image==bg_img3:
        image=bg_img4
    elif image==bg_img4:
        image=bg_img
    return image
pygame.init()
pygame.font.init()
# definiranje konstanti za velicinu prozora
WIDTH = 1600
HEIGHT = 900
RED=[255,0,0]
GREEN=[0,255,0]
BLUE=[0,0,255]
TIRKIZ=[64,224,208]
tamnocijan=[0,139,139]
magenta=[255,0,255]

bg_img=pygame.image.load("slika.jpg")
bg_img=pygame.transform.scale(bg_img,(WIDTH,HEIGHT))
bg_img1=pygame.image.load("slika1.jpg")
bg_img1=pygame.transform.scale(bg_img1,(WIDTH,HEIGHT))
bg_img2=pygame.image.load("slika2.jpg")
bg_img2=pygame.transform.scale(bg_img2,(WIDTH,HEIGHT))
bg_img3=pygame.image.load("slika3.jpg")
bg_img3=pygame.transform.scale(bg_img3,(WIDTH,HEIGHT))
bg_img4=pygame.image.load("slika4.jpg")
bg_img4=pygame.transform.scale(bg_img4,(WIDTH,HEIGHT))
# tuple velicine prozora
size = (WIDTH, HEIGHT)
 
#definiranje novog ekrana za igru
screen = pygame.display.set_mode(size)
#definiranje naziva prozora
pygame.display.set_caption("Auti i putovanje")
myfont = pygame.font.SysFont('Arial', 40)
kodiaq = myfont.render('Provjereni turbodizelaš od 150 KS uvjerljivo pogoni 1,7 tona tešku karoseriju', False, RED)
fabia = myfont.render('Nova redizajnirana Škoda Fabia za samo 85.900kn!', False, RED)
cupra = myfont.render('Od 0-100 za 5.4 sekunde', False, RED)
velebit = myfont.render('Lijep krajolik za kampiranje', False, RED)
velebit1 = myfont.render('Povoljno za off road vožnju', False, RED)
clock = pygame.time.Clock()
state="1"
i=0
duration=10000
bg_color=tamnocijan
bg=bg_img
done = False
while not done:
    #event petlja
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            done = True
        if event.type ==pygame.KEYDOWN:
            if event.key==pygame.K_SPACE:
                bg=next_img(bg)
                     
    #i+=1
    if duration<=0:
        if state=="1":
            state="2"
            duration=10000
        elif state=="2":
            state="3"
            duration=10000
        elif state=="3":
            state="4"
            duration=10000
        elif state=="4":
            state="5"
            duration=10000
        elif state=="5":
            state="1"
            duration=10000
    if state =="1": 
        screen.blit(bg_img, (0, 0))
        screen.blit(kodiaq, (30, 30))
    elif state=="2":
        screen.blit(bg_img1, (0, 0))
        screen.blit(fabia, (30, 30))
    elif state=="3":
        screen.blit(bg_img2, (0, 0))
        screen.blit(cupra, (30, 30))
    elif state=="4":
        screen.blit(bg_img3, (0, 0))
        screen.blit(velebit, (30, 30))
    elif state=="5":
        screen.blit(velebit1, (0, 0))
        screen.blit(bg_img4, (30, 30))
    

    pygame.display.flip()
   
    #ukoliko je potrebno ceka do iscrtavanja
    #iduceg framea kako bi imao 60fpsa
    time= clock.tick(60)
    duration=duration-time
    #print(duration)
pygame.quit()