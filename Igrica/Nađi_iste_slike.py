#Load modules and initialize display
import random, time, pygame
pygame.init()

WIDTH = 100
HEIGHT = 100
CARD_MARGIN=10
CARD_LEN=100
CARD_HOR=37
CARD_VER=22
pygame.display.set_caption("Memory")
size = (630, 360)
screen = pygame.display.set_mode(size)

WHITE = (255, 255, 255)
BLACK = (0, 0, 0)
RED = (255, 0, 0)
GREEN = (0, 255, 0)
font100 = pygame.font.SysFont("Arial", 100)
font50 = pygame.font.SysFont("Arial", 50)
font35 = pygame.font.SysFont("Arial", 35)
font20 = pygame.font.SysFont("Arial", 20)
card_hor = 37
card_ver = 22
ROWS = 3
COLS = 4

cards = [i for i in range(6) for j in range(2)]
random.shuffle(cards)
card_num_grid=[cards[i*len(cards) // ROWS:(i+1)*len(cards) // ROWS] for i in range(ROWS)]
card_grid=[[] for i in range(ROWS)]
for i in range(ROWS):
    if i == 0:
        for j in range(COLS):
            if j == 0:
                card_grid[i].append(pygame.Rect(CARD_MARGIN, CARD_MARGIN, CARD_LEN, CARD_LEN))
                
            else:
                card_grid[i].append(pygame.Rect(card_grid[i][j-1].x + CARD_LEN + CARD_MARGIN, CARD_MARGIN, CARD_LEN, CARD_LEN))                
    else:
        for j in range(COLS):
            if j == 0:
                card_grid[i].append(pygame.Rect(CARD_MARGIN, card_grid[i-1][0].y + CARD_LEN + CARD_MARGIN, CARD_LEN, CARD_LEN))
            else:
                card_grid[i].append(pygame.Rect(card_grid[i][j-1].x + CARD_LEN + CARD_MARGIN, card_grid[i-1][0].y + CARD_LEN + CARD_MARGIN, CARD_LEN, CARD_LEN))
global exposed
exposed = []
global matched
matched = []
global wrong
wrong = []
global turns
turns = 0
done=False
#Game loop
while not done:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            done=True
        elif event.type==pygame.MOUSEBUTTONDOWN:
            mouse_pos=pygame.mouse.get_pos()
            column=mouse_pos[0] // (WIDTH+CARD_MARGIN)
            row=mouse_pos[1] // (HEIGHT+CARD_MARGIN)
            exposed.append([row, column])
    if len(exposed) == 2:
        turns += 1
        if card_num_grid [exposed[0][0]][exposed[0][1]] == card_num_grid [exposed[1][0]][exposed[1][1]]:
            matched.extend(exposed)
            exposed.clear()
        else:
            wrong.extend(exposed)
            exposed.clear()

    screen.fill(BLACK)

    for i in range(ROWS):
        for j in range(COLS):
            color = WHITE
            pygame.draw.rect(screen,color,card_grid[i][j])

    if exposed:
        for i in exposed:
            text=str(card_num_grid[i[0]][i[1]])
            render = font50.render(text, True, BLACK)
            screen.blit(render,(card_grid[i[0]][i[1]].x + card_hor, card_grid[i[0]][i[1]].y + card_ver))
            
    if matched:
        for i in matched:
            text=str(card_num_grid[i[0]][i[1]])
            render = font50.render(text, True, GREEN)
            screen.blit(render,(card_grid[i[0]][i[1]].x + card_hor, card_grid[i[0]][i[1]].y + card_ver))

    if wrong:
        for i in wrong:
            text=str(card_num_grid[i[0]][i[1]])
            render = font50.render(text, True, RED)
            screen.blit(render,(card_grid[i[0]][i[1]].x + card_hor, card_grid[i[0]][i[1]].y + card_ver))

    title = font35.render("Memory", True, WHITE)
    screen.blit(title, (450, 10))
    turn_text = font20.render("Turns: " + str(turns), True, WHITE)
    screen.blit(turn_text, (460, 75))

    if len(matched) == 12:
        screen.fill(BLACK)
        win = font100.render("You win!", True, GREEN)
        screen.blit(win, (20, 105))
        pygame.display.flip()
        break
    
    pygame.display.flip()
    if wrong:
        time.sleep(0.5)
        wrong.clear()
pygame.quit()