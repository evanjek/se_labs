# ukljucivanje biblioteke pygame
import pygame

def next_img(image):
    if image==bg_img:
        image=bg_img1
    elif image==bg_img1:
        image=bg_img2
    elif image==bg_img2:
        image=bg_img3
    elif image==bg_img3:
        image=bg_img4
    elif image==bg_img4:
        image=bg_img5
    elif image==bg_img5:
        image=bg_img6
    elif image==bg_img6:
        image=bg_img7
    elif image==bg_img7:    
        image=bg_img
    return image
pygame.init()
# definiranje konstanti za velicinu prozora
WIDTH = 1500
HEIGHT = 600
RED=[255,0,0]
GREEN=[0,255,0]
BLUE=[0,0,255]
TIRKIZ=[64,224,208]
tamnocijan=[0,139,139]
magenta=[255,0,255]

bg_img=pygame.image.load("bg.png")
bg_img=pygame.transform.scale(bg_img,(WIDTH,HEIGHT))
bg_img1=pygame.image.load("bg1.png")
bg_img1=pygame.transform.scale(bg_img1,(WIDTH,HEIGHT))
bg_img2=pygame.image.load("bg2.png")
bg_img2=pygame.transform.scale(bg_img2,(WIDTH,HEIGHT))
bg_img3=pygame.image.load("bg3.png")
bg_img3=pygame.transform.scale(bg_img3,(WIDTH,HEIGHT))
bg_img4=pygame.image.load("bg4.png")
bg_img4=pygame.transform.scale(bg_img4,(WIDTH,HEIGHT))
bg_img5=pygame.image.load("bg5.png")
bg_img5=pygame.transform.scale(bg_img5,(WIDTH,HEIGHT))
bg_img6=pygame.image.load("bg6.png")
bg_img6=pygame.transform.scale(bg_img6,(WIDTH,HEIGHT))
bg_img7=pygame.image.load("bg7.png")
bg_img7=pygame.transform.scale(bg_img7,(WIDTH,HEIGHT))
# tuple velicine prozora
size = (WIDTH, HEIGHT)
 
#definiranje novog ekrana za igru
screen = pygame.display.set_mode(size)
#definiranje naziva prozora
pygame.display.set_caption("Nasa kul igra")
 
clock = pygame.time.Clock()
i=0
duration=1500
bg_color=tamnocijan
bg=bg_img
done = False
while not done:
    #event petlja
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            done = True
        if event.type ==pygame.KEYDOWN:
            if event.key==pygame.K_SPACE:
                bg=next_img(bg)               
    i+=1
    if duration<0:
        duration=1000
        bg=next_img(bg)
        

    screen.fill(bg_color)
    screen.blit(bg,(0,0))
    pygame.display.flip()
   
    #ukoliko je potrebno ceka do iscrtavanja
    #iduceg framea kako bi imao 60fpsa
    time= clock.tick(60)
    duration=duration-time
    #print(duration)
pygame.quit()