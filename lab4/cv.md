  OSOBNI PODACI

Ime i  prezime:
Richard Mark Hammond

Datum i mjesto rođenja:
19. Prosinca 1969., England


POSAO:
 
 Radio u Top Gear-u od 2002. do 2015. sa Jeremy Clarkson i James May. 
 Godine 2016. Hammond je počeo predstavljati televizijsku seriju Grand Tour, u produkciji W. Chump & Sons. Emisija je zajedno predstavljena s bivšim suparnicima Top Gear-a, Clarksonom i Mayom, kao ekskluzivni način distribucije putem Amazon Video-a kupcima Amazon Prime.

OSOBNI ŽIVOT:

Richard Hammond oženjen je Amandom "Mindy" Hammond od svibnja 2002. Imaju dvije kćeri. 

Hammond svira bas gitaru na kojoj je pratio ostale izlagače Top Gear-a kada su zajedno sa Justinom Hawkinsom nastupili na Top Gear of the Pops for Comic Relief 2007. 

Voli voziti bicikl, skuter ili motocikl po gradovima zbog čega ga nemilosrdno ismijava kolega voditelj Jeremy Clarkson.
